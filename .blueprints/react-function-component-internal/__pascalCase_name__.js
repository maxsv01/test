import React from 'react';
import PropTypes from 'prop-types';

function {{pascalCase name}}(props) {
  return (
    <div />
  );
}

{{pascalCase name}}.propTypes = {};

{{pascalCase name}}.defaultProps = {};

export default {{pascalCase name}};