import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import { pickHTMLProps } from 'pick-react-known-prop';

const useStyles = makeStyles((/* theme */) => ({
 root: {},
}), { name: '{{pascalCase name}}' });

function {{pascalCase name}}({ classes: pClasses, className, ...rest }) {
  const classes = useStyles({ classes: pClasses });
  return (
    <div {...pickHTMLProps(rest)} className={clsx(classes.root, className)}>

    </div>
  );
}

{{pascalCase name}}.propTypes = {
  classes: PropTypes.shape({
    root: PropTypes.string,
  }),
  className: PropTypes.string,
};

{{pascalCase name}}.defaultProps = {
  classes: undefined,
  className: undefined,
};

export default {{pascalCase name}};
