import React from 'react';
import PropTypes from 'prop-types';

function {{pascalCase name}}({ children }) {
  return (
    <>
      {children}
    </>
  );
}

{{pascalCase name}}.propTypes = {
  children: PropTypes.node,
};

{{pascalCase name}}.defaultProps = {
  children: null,
};

export default {{pascalCase name}};
