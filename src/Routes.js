import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom';
import Calculations from './components/Calculations';
import Home from './components/Home';

function Routes({ children, ...rest }) {
  const nocValue = localStorage.getItem('nocValue');

  return (
    <Router {...rest}>
      {(nocValue === '' || nocValue === undefined || nocValue === null) && <Redirect to="/" />}
      {!(nocValue === '' || nocValue === undefined || nocValue === null) && (
        <Redirect to="/calculations" />
      )}
      {children}
      <Route exact path="/" component={Home} />
      <Route exact path="/calculations" component={Calculations} />
    </Router>
  );
}

export default Routes;
