import React from 'react';
import { ThemeProvider } from '@material-ui/core/styles';
import theme from './theme';
import AppMenu from './components/AppMenu';
import Routes from './Routes';
import AccessContext from './access/AccessContext';

function App() {
  const [access, setAccess] = React.useState(false);
  const [nocValue, setNocValue] = React.useState('');

  const toggleAccess = React.useCallback(() => {
    setAccess((prev) => !prev);
  }, []);

  const value = React.useMemo(() => ({ access, toggleAccess, nocValue, setNocValue }), [
    access,
    toggleAccess,
    nocValue,
    setNocValue,
  ]);

  return (
    <ThemeProvider theme={theme}>
      <AccessContext.Provider value={value}>
        <Routes>
          <AppMenu />
        </Routes>
      </AccessContext.Provider>
    </ThemeProvider>
  );
}

export default App;
