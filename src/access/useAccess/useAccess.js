import React from 'react';
import AccessContext from '../AccessContext';

function useAccess() {
  return React.useContext(AccessContext);
}

export default useAccess;
