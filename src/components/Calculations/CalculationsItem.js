import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import { pickHTMLProps } from 'pick-react-known-prop';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles(
  (theme) => ({
    root: {
      ...theme.typography.body1,
      fontSize: '1.2rem',
      padding: theme.spacing(2),
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      border: '1px solid',
      borderColor: theme.palette.grey[400],
      borderRadius: theme.spacing(1),
      width: '100%',
      maxWidth: '20%',
      margin: theme.spacing(1),
      backgroundColor: theme.palette.background.paper,
    },
    value: {
      paddingRight: theme.spacing(1),
    },
    textfield: {
      maxWidth: theme.spacing(17),
    },

    success: {
      borderColor: 'rgba(129, 199, 132, 0.3)',
      backgroundColor: 'rgba(129, 199, 132, 0.3)',
    },
    error: {
      borderColor: 'rgba(229, 115, 115, 0.3)',
      backgroundColor: 'rgba(229, 115, 115, 0.3)',
    },
  }),
  { name: 'CalculationsItem' },
);

const CalculationsItem = React.forwardRef(function CalculationsItem(
  { classes: pClasses, className, data, result, disabled, value, handleChangeResult, id, ...rest },
  ref,
) {
  const classes = useStyles({ className, classes: pClasses });

  return (
    <div
      {...pickHTMLProps(rest)}
      className={clsx(classes.root, className, {
        [classes.success]: result === true,
        [classes.error]: result === false,
      })}
    >
      <div className={classes.value}>{data} = </div>
      <TextField
        label="Result"
        // autoFocus
        type="text"
        variant="outlined"
        className={classes.textfield}
        size="small"
        // disabled={disabled}
        id={id}
        value={value}
        onChange={handleChangeResult}
        ref={ref}
      />
    </div>
  );
});

CalculationsItem.propTypes = {
  classes: PropTypes.shape({
    root: PropTypes.string,
    success: PropTypes.string,
    textfield: PropTypes.string,
    value: PropTypes.string,
  }),
  // disabled: PropTypes.bool.isRequired,
  result: PropTypes.bool.isRequired,
  data: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  value: PropTypes.string,
  // handleChangeResult: PropTypes.func.isRequired,
  className: PropTypes.string,
};

CalculationsItem.defaultProps = {
  value: undefined,
  classes: undefined,
  className: undefined,
};

export default CalculationsItem;
