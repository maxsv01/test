import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import Page from '../Page';
// import useAccess from '../../access/useAccess';
import Button from '@material-ui/core/Button';
import { Redirect } from 'react-router-dom';
import CalculationsItem from './CalculationsItem';

const useStyles = makeStyles(
  (theme) => ({
    root: {
      boxShadow: theme.shadows[2],
      borderBottomRightRadius: theme.spacing(1),
      borderBottomLeftRadius: theme.spacing(1),
    },
    container: {
      display: 'flex',
      justifyContent: 'center',
      flexWrap: 'wrap',
      paddingBottom: theme.spacing(3),
    },
    btn: {
      marginLeft: theme.spacing(5),
    },
  }),
  { name: 'Calculations' },
);

function Calculations({ classes: pClasses, className, ...rest }) {
  const classes = useStyles({ classes: pClasses });
  // const { access } = useAccess();
  const nocValue = localStorage.getItem('nocValue');

  const [goHome, setGoHome] = React.useState(false);
  const [inputValue, setInputValue] = React.useState([]);
  const [result, setResult] = React.useState(false);

  // const ref = React.createRef('');

  const inputRef = React.useRef();
  // inputRef.current = [];

  // const addToRefs = (el) => {
  //   if (el && inputRef.current.includes(el)) {
  //     inputRef.current.push(el);
  //   }
  //   // console.log(inputRef.current);
  //   // console.log(el);
  // };

  const handleClickHome = React.useCallback(() => {
    localStorage.clear();
    setGoHome(true);
  }, []);

  const params = {
    min: 1,
    max: 10,
    operator: ['+', '-', '*'],
  };

  const { min, max, operator } = params;

  const getRandomIntInclusive = React.useCallback((min, max) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }, []);

  const getRandomOperator = React.useCallback((operator) => {
    return operator[Math.floor(Math.random() * operator.length)];
  }, []);

  const getRandomValue = React.useCallback(() => {
    const firstValue = getRandomIntInclusive(min, max);
    const secondValue = getRandomIntInclusive(min, max);
    const operatorValue = getRandomOperator(operator);

    const value = firstValue + operatorValue + secondValue;
    return value;
  }, [getRandomIntInclusive, getRandomOperator, min, max, operator]);

  const setValuesToArray = React.useCallback(
    (nocValue) => {
      const arrayValuesStorage = [];
      for (let i = 0; i < nocValue; i++) {
        arrayValuesStorage.push(getRandomValue());
        localStorage.setItem('arrayValues', JSON.stringify(arrayValuesStorage));
      }
    },
    [getRandomValue],
  );

  const arrayValuesLocalStorage = JSON.parse(localStorage.getItem('arrayValues'));
  if (!arrayValuesLocalStorage) setValuesToArray(nocValue);

  const resultArray = [];

  arrayValuesLocalStorage?.length > 0 &&
    (arrayValuesLocalStorage || []).map((i) => {
      return resultArray.push(eval(i)); // eslint-disable-line no-eval
    });

  localStorage.setItem('resultArray', JSON.stringify(resultArray));

  const resultArrayLocalStorage = JSON.parse(localStorage.getItem('resultArray'));

  const handleChangeInput = React.useCallback(
    (e) => {
      const target = e.target;
      const value = target.value;
      const id = target.id;
      setInputValue({
        inputValue: inputValue.map((i, index) => {
          if (index === id) {
            return {
              ...i,
              [id]: value,
            };
          }
          return id;
        }),
      });
      // tempArray.push(inputValue);
      // console.log('e', e.target.id);

      // const value = e.target.value;

      // inputResult.push(value);
      // setInputValue(e.target.value);
      // inputResult.push(inputRef.current.value);
      // console.log('inputResult', inputResult);
    },
    [inputValue],
  );
  console.log('inputValue', inputValue);

  // console.log('addToRefs', addToRefs);
  return (
    <Page {...rest} className={clsx(classes.root, className)}>
      <div className={classes.container}>
        {arrayValuesLocalStorage?.length > 0 &&
          (arrayValuesLocalStorage || []).map((i, index) => {
            // console.log('inputValue[index]', Object.values(inputValue || {}));
            return (
              <CalculationsItem
                key={index.toString()}
                id={index.toString()}
                data={i}
                value={inputValue[index] || ''}
                handleChangeResult={handleChangeInput}
                result={result}
                // ref={inputRef.current[index] || []}
              />
            );
          })}
      </div>
      <Button
        onClick={handleClickHome}
        variant="contained"
        size="large"
        color="primary"
        className={classes.btn}
      >
        Go Home
      </Button>
      {goHome && <Redirect to="/" />}
    </Page>
  );
}

Calculations.propTypes = {
  classes: PropTypes.shape({
    btn: PropTypes.string,
    container: PropTypes.string,
    root: PropTypes.string,
  }),
  className: PropTypes.string,
};

Calculations.defaultProps = {
  classes: undefined,
  className: undefined,
};

export default Calculations;
