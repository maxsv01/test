import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';

const useStyles = makeStyles(
  (theme) => ({
    root: { padding: theme.spacing(4, 0), marginBottom: theme.spacing(4) },
  }),
  { name: 'Page' },
);

function Page({ classes: pClasses, className, children, maxWidth }) {
  const classes = useStyles({ classes: pClasses });
  return (
    <Container maxWidth={maxWidth} className={clsx(classes.root, className)}>
      {children}
    </Container>
  );
}

Page.propTypes = {
  classes: PropTypes.shape({
    root: PropTypes.string,
  }),
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
  maxWidth: PropTypes.string,
};

Page.defaultProps = {
  classes: undefined,
  className: undefined,
  maxWidth: 'lg',
};

export default Page;
