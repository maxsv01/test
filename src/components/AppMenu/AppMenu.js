import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import { pickHTMLProps } from 'pick-react-known-prop';
import { Link, useLocation } from 'react-router-dom';

const useStyles = makeStyles(
  (theme) => ({
    root: {
      ...theme.typography.h3,
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      padding: theme.spacing(2, 0),
      boxShadow: theme.shadows[1],
      [theme.breakpoints.down('xs')]: {
        fontSize: '1.5rem',
      },
    },
    link: {
      margin: theme.spacing(0, 2),
      textDecoration: 'none',
      color: theme.palette.primary.main,
    },
    disabledLink: {
      color: theme.palette.text.secondary,
    },
    activeLink: {
      borderBottom: '4px solid',
      borderBottomColor: theme.palette.primary.main,
    },
  }),
  { name: 'AppMenu' },
);

function AppMenu({ classes: pClasses, className, ...rest }) {
  const classes = useStyles({ classes: pClasses });

  const location = useLocation();

  return (
    <div {...pickHTMLProps(rest)} className={clsx(classes.root, className)}>
      {location.pathname === '/' && (
        <Link
          to="/"
          className={clsx(classes.link, { [classes.activeLink]: location.pathname === '/' })}
        >
          Home
        </Link>
      )}
      {location.pathname === '/calculations' ? (
        <>
          <span className={clsx(classes.link, classes.disabledLink)}>Home</span>
          <Link
            to="/calculations"
            className={clsx(classes.link, {
              [classes.activeLink]: location.pathname === '/calculations',
            })}
          >
            Calculations
          </Link>
        </>
      ) : (
        <span className={clsx(classes.link, classes.disabledLink)}>Calculations</span>
      )}
    </div>
  );
}

AppMenu.propTypes = {
  classes: PropTypes.shape({
    disabledLink: PropTypes.string,
    link: PropTypes.string,
    root: PropTypes.string,
  }),
  className: PropTypes.string,
};

AppMenu.defaultProps = {
  classes: undefined,
  className: undefined,
};

export default AppMenu;
