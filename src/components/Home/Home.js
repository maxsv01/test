import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { Redirect } from 'react-router-dom';
import useAccess from '../../access/useAccess';
import Page from '../Page';

const useStyles = makeStyles(
  (theme) => ({
    root: {
      '& .MuiTextField-root': {
        margin: theme.spacing(1),
        width: 200,
      },
    },
    container: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'column',
    },
    btn: {
      marginTop: theme.spacing(2.5),
    },
  }),
  { name: 'Home' },
);

function Home({ classes: pClasses, className, ...rest }) {
  const classes = useStyles({ classes: pClasses });

  const { toggleAccess } = useAccess();

  const [noc, setNoc] = React.useState('');
  const [disableBtn, setDisableBtn] = React.useState(true);
  const [redirect, setRedirect] = React.useState(false);
  const [error, setError] = React.useState(false);

  const isValid = (value) => {
    const reg = new RegExp('(^[0-9]+$|^$)');
    const validRegExp = reg.test(value);

    return validRegExp;
  };

  const handleChangeNoc = React.useCallback(
    (e) => {
      const value = e.target.value;
      if (isValid(value)) {
        setNoc(() => value);
        const param = {
          min: 20,
          max: 60,
        };
        if (value >= param.min && value <= param.max) {
          setError(false);
          setDisableBtn(false);
          localStorage.setItem('nocValue', value);
          return value;
        } else {
          setError(true);
          setDisableBtn(true);
          if (error && !(value >= param.min && value <= param.max)) {
            setError(true);
            setDisableBtn(true);
          }
          if (value === '') {
            setError(false);
            setDisableBtn(true);
          }
        }
      }
    },
    [error],
  );
  const handleSubmit = React.useCallback(
    (e) => {
      e.preventDefault();
      toggleAccess(true);
      setRedirect(true);
    },
    [toggleAccess],
  );

  return (
    <Page className={clsx(classes.root, className)} {...rest}>
      <form className={classes.container} onSubmit={handleSubmit}>
        <TextField
          required
          label="NoC"
          variant="outlined"
          autoFocus
          helperText="20 <= NoC <= 60"
          inputProps={{ maxLength: '2' }}
          type="text"
          value={noc}
          error={error}
          onChange={handleChangeNoc}
        />
        <Button
          variant="contained"
          color="primary"
          className={classes.btn}
          type="submit"
          disabled={disableBtn}
        >
          Start
        </Button>
        {redirect && <Redirect to="/calculations" />}
      </form>
    </Page>
  );
}

Home.propTypes = {
  classes: PropTypes.shape({
    btn: PropTypes.string,
    container: PropTypes.string,
    root: PropTypes.string,
  }),
  className: PropTypes.string,
};

Home.defaultProps = {
  classes: undefined,
  className: undefined,
};

export default Home;
